const Helper = require('./helper');
const UI = require('./ui');
const Discord = require('discord.js');

function init() {
    return new Promise(async function(resolve) {
        const token = Helper.getConfigValue('token');
        if (!token) {
            Helper.prompt("Enter token: ").then(async function(input) {
                let client = await validToken(input);
                if (!client) {
                    await init();
                } else {
                    Helper.setConfigValue('token', input);
                    resolve(client);
                }
            });
        } else {
            let client = await validToken(token);
            if (!client) {
                await init();
            } else {
                resolve(client);
            }
        }
    });
}
function validToken(token) {
    return new Promise(function(resolve) {
        console.log('Logging in...');
        let client = new Discord.Client();
        client.login(token).then(async function() {
            resolve(client);
        }).catch(function() {
            console.log('Invalid token!');
            resolve(false);
        });
    });
}

let started = false;
let guild = null;
let channel = null;

async function login(client) {
    if (started) return;
    if (client.status !== 0) {
        client.on('ready', function() {
            login(client);
        });
        return;
    }
    started = true;
    console.clear();

    const cGuild = Helper.getConfigValue('last_guild');
    const cChannel = Helper.getConfigValue('last_channel');

    if (client.guilds.size === 0) {
        console.log('Join a discord server first!');
        process.exit(0);
    }

    guild = client.guilds.find(function(g) {
        return g.id === cGuild;
    });
    channel = guild ? guild.channels.find(function(c) {
        return c.id === cChannel;
    }) : null;

    if (!guild) {
        guild = client.guilds.first();
    }
    if (!channel || ['voice', 'category'].includes(channel.type)) {
        channel = guild.channels.find(function(c) {
            return c.type !== 'category' && c.type !== 'voice';
        });
    }

    Helper.setConfigValue('last_guild', guild.id);
    Helper.setConfigValue('last_channel', channel.id);

    const messages = await channel.fetchMessages();

    UI.drawUI(guild, channel);

    UI.oninput(async function(input) {
        if (input) {
            await channel.send(input);
        }
    });

    let loadedMessages = [];
    messages.tap(function(message) {
        if (!message.cleanContent.trim()) {
            return;
        }
        loadedMessages.push([message.member, message.cleanContent.trim()]);
    });
    loadedMessages = loadedMessages.reverse();
    loadedMessages.forEach(function(v) {
        UI.addNewMessage(v[0], v[1]);
    });

    client.on('message', function(message) {
        if (!message.cleanContent.trim() || message.guild.id !== guild.id || message.channel.id !== channel.id || message.author.id === client.user.id) {
            return;
        }
        UI.addNewMessage(message.member, message.cleanContent.trim());
    });
}

module.exports = {
    init: init,
    login: login
};