const fs = require('fs');
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function loadJSON(file) {
    try {
        const contents = fs.readFileSync(file).toString();
        return JSON.parse(contents);
    } catch(e) {}
    return null;
}
function saveJSON(file, json) {
    fs.writeFileSync(file, JSON.stringify(json));
}
function setConfigValue(key, value) {
    let config = loadJSON('.discoterm');
    if (!config) {
        config = {};
    }
    config[key] = value;
    saveJSON('.discoterm', config);
}
function getConfigValue(key) {
    let config = loadJSON('.discoterm');
    if (!config) {
        config = {};
    }
    return key in config ? config[key] : null;
}

function prompt(question) {
    return new Promise(function(resolve) {
        rl.question(question, function(response) {
            resolve(response);
        });
    });
}
module.exports = {
    loadJSON: loadJSON,
    saveJSON: saveJSON,
    prompt: prompt,
    setConfigValue: setConfigValue,
    getConfigValue: getConfigValue
};