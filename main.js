const DiscoTerm = require('./disco');

DiscoTerm.init().then(async function(client) {
    await DiscoTerm.login(client);
});

process.on('SIGINT', function() {
    process.exit();
});