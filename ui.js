const readline = require('readline');

let inputHandler = null;
let rl = null;

function drawUI(guild, channel) {
    console.clear();
    rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
        terminal: false
    });

    rl.on('line', function(line) {
        if (inputHandler) {
            inputHandler(line.trim());
        }
        rl.prompt(true);
    });
}
function addNewMessage(user, msg, usercolor) {
    rl.pause()
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    console.log(user.displayName + ': ' + msg.padEnd(10, ' '));
    rl.prompt(true);
    rl.resume();
}
function oninput(func) {
    inputHandler = func;
}

module.exports = {
    drawUI: drawUI,
    addNewMessage: addNewMessage,
    oninput: oninput
};